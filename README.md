# dmaland0's JSUtilities

JSUtilities is a "library" (really just a Javascript object) with functionalities that add useful behaviors and automate repetitive tasks. There is an emphasis on compatibility, meaning that basic Javascript is used as much as possible. Some of the functionality can indeed be found in the latest and greatest Javascript implementations, but those implementations might not work correctly in one browser or another. I can't guarantee, of course, that every single browser will play nicely with JSUtilities, but the ones that might have a problem should be few and far between.

Exactly how you use JSUtilities is up to you and your projects. As I said before, it's really just a plain object that - without modification by you - gets defined as a global constant when JSUtilities.js is loaded and executed. That may not be a best practice when certain frameworks are in play; That's up to you to decide.

The returnUtilities function (and subsquent call) was put in mainly to make the library work automatically with testDriver.js.

TestDriver.js can be run if you have NodeJS available. It's mostly for my own use, but you might find it helpful as a "playground" for getting a handle on how JSUtilities behaves.

## JSUtilities is one of my various projects that I work on to - hopefully - make the world a tiny bit better.

Support me on Patreon, please: https://www.patreon.com/danielmaland
