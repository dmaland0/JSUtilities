//-----Dependencies
//Builtin And Third-Party Modules
const fs = require('fs');
//Custom Modules
//-----

var code = fs.readFileSync('./JSUtilities.js', 'utf-8');
var JSUtilities = null;

if (code) {
	JSUtilities = eval(code);
}

const u = JSUtilities;

//**********
//Configure which tests to run here...
var test = {
	forIn: true,
	valueIsIn: true,
	emailSeemsValid: true,
	makeSQLDatetime: true,
	iterableParser: true,
	stripAllButLetters: true,
	stripAllButNumbers: true,
	stripAllButNumericalFormatCharacters: true,
	stripSpecifiedFromInput: true,
	formatPhoneNumber: true,
	length: true
};
//**********

var arr = [1,2,3,4,5];
var obj = {name: 'stuff', number: 5};
var anotherObj = {name: 'moreStuff', number: 6};
var objArr = [obj, anotherObj];

if (test.forIn) {

	console.log("\nTesting nested iteration through an array, with an iteration through an object at each step:\n");

	u.forIn(arr, function(index, element) {
	  console.log(index, element);
	  u.forIn(obj, function(prop,value) {
	  	console.log(prop, value);
	  });
	});

	console.log("\nTesting iteration driven by an integer:\n");

	u.forIn(10, function(index) {
	  console.log(index);
	});

	console.log("\nTesting iteration through a section of an array:\n");

	u.forIn(arr, function(index, element) {
			console.log(index, element);
		},
		{start:1,stop:3});

}

if (test.valueIsIn) {

	console.log("\nTesting whether a value is in an array:\n");

	console.log(u.valueIsIn(3, arr));

	console.log("\nTesting whether a value is in an object:\n");

	console.log(u.valueIsIn('stuff', obj));

	console.log("\nTesting whether an object is in an array:\n");

	console.log(u.valueIsIn(obj, objArr));

	console.log("\nTesting what happens when the collection is a string:\n");

	console.log(u.valueIsIn("g", "abcdefg"));

	console.log("\nTesting what happens when the value isn't found:\n");

	console.log(u.valueIsIn('stuff', arr));

	console.log("\nTesting what happens with an invalid iterable:\n");

	console.log(u.valueIsIn('stuff', 42));

}

if (test.emailSeemsValid) {

	console.log("\nTesting whether person@gmail.com is an email:\n");

	console.log(u.emailSeemsValid("person@gmail.com"));

	console.log("\nTesting whether per son@gmail.com is an email:\n");

	console.log(u.emailSeemsValid("per son@gmail.com"));

	console.log("\nTesting whether personatgmail.com is an email:\n");

	console.log(u.emailSeemsValid("personatgmail.com"));

	console.log("\nTesting whether person@gm ail.com is an email:\n");

	console.log(u.emailSeemsValid("personatgm ail.com"));

	console.log("\nTesting whether person@gmail.c om is an email:\n");

	console.log(u.emailSeemsValid("personatgmail.c om"));

	console.log("\nTesting whether person@gmail.co is an email:\n");

	console.log(u.emailSeemsValid("person@gmail.co"));

}

if (test.makeSQLDatetime) {

	console.log("\nTesting converting the current time to SQL datetime:\n");

	console.log(u.makeSQLDatetime(new Date()));

	console.log("\nTesting converting 2017-1-1 1:1:1 to a SQL datetime:\n");

	console.log(u.makeSQLDatetime(new Date("2017-1-1 1:1:1")));

	console.log("\nTesting converting an invalid date to a SQL datetime:\n");

	console.log(u.makeSQLDatetime(new Date("q")));
	
}

if (test.iterableParser) {
	
	console.log("\nTesting the parsing of an object:\n");

	console.log(u.iterableParser(obj));

	console.log("\nTesting the parsing of an array:\n");

	console.log(u.iterableParser(arr));

	console.log("\nTesting the parsing of a string:\n");

	console.log(u.iterableParser("string"));

	console.log("\nTesting iterableParser with an integer:\n");

	console.log(u.iterableParser(10));
	
}

if (test.stripAllButLetters) {

	console.log("\nTesting the removal of everything except alphabet characters from 'Jarthur[].98++IsNotAReal,<Name':\n");

	console.log(u.stripAllButLetters("Jarthur[].98++IsNotAReal,<Name"));

}

if (test.stripAllButNumbers) {

	console.log("\nTesting the removal of everything except number characters from 'Jar64thur[].98++IsNotAReal,<Na12me':\n");

	console.log(u.stripAllButNumbers("Jar64thur[].98++IsNotAReal,<Na12me"));

	console.log("\nTesting the removal of everything except number characters from a float variable (68.945):\n");

	var number = 68.945;

	console.log(u.stripAllButNumbers(number));

}

if (test.stripAllButNumericalFormatCharacters) {

	console.log("\nTesting the removal of everything except numerical format characters from 'Jar64thur[].98++IsNotAReal,<Na12me':\n");

	console.log(u.stripAllButNumericalFormatCharacters("Jar64thur[].98++IsNotAReal,<Na12me"));

}

if (test.stripSpecifiedFromInput) {

	console.log("\nTesting the removal of characters a-j from 'bananaj':\n");

	console.log(u.stripSpecifiedFromInput("a-j", "bananaj"));

	console.log("\nTesting the removal of characters !,*,$ from '!person*@$email.$com':\n");

	console.log(u.stripSpecifiedFromInput("!*$", "!person*@$email.$com"));

}

if (test.formatPhoneNumber) {

	console.log("\nTesting phone number formatting of '555.555.1234':\n");

	console.log(u.formatPhoneNumber("555.555.1234"));

	console.log("\nTesting phone number formatting of '(555) 555-1234' with a '-' as a separator:\n");

	console.log(u.formatPhoneNumber("(555) 555-1234", "-"));

}

if (test.length) {

	console.log("\nTesting the number of properties in the object called obj (there should be two):\n");

	console.log(u.length(obj));
	
};
